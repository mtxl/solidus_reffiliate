Deface::Override.new(
  :virtual_path => "spree/admin/users/edit",
  :name => "referral_fieldset",
  :insert_after => "[data-hook='admin_user_api_key']"
) do
<<-CODE.chomp
<fieldset data-hook="admin_user_referral_table" class="alpha twelve columns">
  <legend><%= Spree.t(:referral_information) %></legend>
  <table>
    <tr>
      <th><%= Spree.t(:referred_by) %></th>
      <td>
        <% if @user.referred? %>
          <%= link_to(@user.referred_by.email, edit_admin_user_url(@user.referred_by)) %>
          | <%= link_to('Remove', admin_referred_record_path(id: @user.referred_record), method: :delete, remote: false) %>
        <% elsif @user.affiliate? %>
          <%= link_to(@user.affiliate.name, edit_admin_affiliate_url(@user.affiliate)) %>
          | <%= link_to('Remove', admin_referred_record_path(id: @user.referred_record), method: :delete, remote: false) %>
        <% else %>
          Organic
        <% end %>
      </td>
    </tr>
      <th><%= Spree.t(:referral_code) %></th>
      <td><%= @user.referral.code %></td>
    <tr>
    </tr>
      <th><%= Spree.t(:orders_referred) %></th>
      <td>
        <%= Spree.t(:no_referred_orders) if @user.referral.referred_orders.count == 0 %>
        <ol style="margin-left: 20px;">
          <% @user.referral.referred_orders.each do |order| %>
            <li><%= link_to order.number, edit_admin_order_path(order) %> (<%= order.state %>)</li>
          <% end %>
        <ol>
      </td>
    <tr>
    <tr>
      <th><%= Spree.t(:users_referred) %></th>
      <td>
        <%= Spree.t(:no_referred_users) if @user.referred_count == 0 %>
        <ol style="margin-left: 20px;">
          <% @user.referral.referred_users.each do |user| %>
            <li><%= link_to user.email, edit_admin_user_url(user) %></li>
          <% end %>
        </ol>
      </td>
    <tr>
  </tr>
</fieldset>
CODE
end
